import AST.*;
import AST.Module;

import java.io.*;
import java.util.Iterator;
import beaver.Parser;

public class Compiler {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: java -jar Oberon0-Compiler-A2b.jar <filename>");
			System.exit(1);
		}
		
		FileReader reader = null;
		try {
			reader = new FileReader(args[0]);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
		Oberon0Scanner scanner = new Oberon0Scanner(reader);
		Oberon0Parser parser = new Oberon0Parser();

		PrintStream err = System.err;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		try {
			System.setErr(ps);
			Module m = (Module) parser.parse(scanner);
			System.setErr(err);
		
			if (os.size() > 0) {
				System.out.println("parse failed");
				System.exit(1);
			} 

			Iterator<ErrorMessage> itr = m.errors().iterator();
			if (itr.hasNext()) {
				System.out.println(itr.next().getLine());
			}
			
			File file = new File(args[0]);
			String filename = file.getAbsolutePath();
			if (filename.endsWith(".ob")) {
				filename = filename.substring(0, filename.lastIndexOf(".ob"));
				
				StringBuilder errors = new StringBuilder();
				for (ErrorMessage e : m.errors()) {
					errors.append("- ").append(e).append("\n");
				}				
				writeToFile(filename + ".errors", errors.toString());
				
				writeToFile(filename + "_pp.ob", m.prettyPrint());
			} else {
				System.out.println("The input file should have the filename extension .ob");
				System.exit(1);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		} catch (Parser.Exception e) {
			System.out.println("parse failed");
			System.exit(1);
		} finally {
			System.setErr(err);
		}
	}
	
	public static void writeToFile(String filename, String text) {
		try {
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.write(text);
			fileWriter.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
}
