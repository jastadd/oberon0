<YYINITIAL> {
  "#"             { return sym(Terminals.NOT_EQ); }
  "<"             { return sym(Terminals.LT); }
  "<="            { return sym(Terminals.LE); }
  ">"             { return sym(Terminals.GT); }
  ">="            { return sym(Terminals.GE); }

  "+"             { return sym(Terminals.PLUS); }
  "-"             { return sym(Terminals.MINUS); }
  "OR"            { return sym(Terminals.OR); }

  "*"             { return sym(Terminals.MUL); }
  "DIV"           { return sym(Terminals.DIV); }
  "MOD"           { return sym(Terminals.MOD); }
  "&"             { return sym(Terminals.AND); }

  "~"             { return sym(Terminals.NOT); }

  "("             { return sym(Terminals.LPAR); }
  ")"             { return sym(Terminals.RPAR); }
  ":="            { return sym(Terminals.ASSIGN); }
  "="             { return sym(Terminals.EQ); }
  ":"             { return sym(Terminals.COL); }
  ";"             { return sym(Terminals.SCOL); }
  "."             { return sym(Terminals.DOT); }
  ","             { return sym(Terminals.COMMA); }
  "MODULE"        { return sym(Terminals.MODULE); }
  "BEGIN"         { return sym(Terminals.BEGIN); }
  "END"           { return sym(Terminals.END); }
  "WHILE"         { return sym(Terminals.WHILE); }
  "DO"            { return sym(Terminals.DO); }
  "IF"            { return sym(Terminals.IF); }
  "THEN"          { return sym(Terminals.THEN); }
  "ELSIF"         { return sym(Terminals.ELSIF); }
  "ELSE"          { return sym(Terminals.ELSE); }
  "CONST"         { return sym(Terminals.CONST); }
  "TYPE"          { return sym(Terminals.TYPE); }
  "VAR"           { return sym(Terminals.VAR); }

  "FOR"           { return sym(Terminals.FOR); }   // The new FOR loop
  "TO"            { return sym(Terminals.TO); }
  "BY"            { return sym(Terminals.BY); }

  "CASE"          { return sym(Terminals.CASE); }  // The new CASE statement
  "OF"            { return sym(Terminals.OF); }
  "|"             { return sym(Terminals.PIPE); }
  ".."            { return sym(Terminals.DOTDOT); }

  {ID}            { return sym(Terminals.ID); }
  {NUMBER}        { return sym(Terminals.NUMBER); }
  <<EOF>>         { return sym(Terminals.EOF); }
}
