package AST;

import AST.Oberon0Parser.Terminals;

%%

%public
%final
%class Oberon0Scanner 
%extends beaver.Scanner

%type beaver.Symbol 
%function nextToken 
%yylexthrow beaver.Scanner.Exception

%state COMMENT

%line
%column
%{
  // To support nested comments
  int commentLevel = 0; 

  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n

ID = [a-zA-Z][a-zA-Z0-9]*
NUMBER = [0-9]*


%%

<COMMENT> {
  "(*"           { commentLevel++; }
  "*)"           { commentLevel--;
                    if (commentLevel == 0) 
                      yybegin(YYINITIAL);
                 }
  .|\n           { }
}

<YYINITIAL> {
  "(*"              { commentLevel = 1; yybegin(COMMENT);}
  {WhiteSpace}      { } 
}
