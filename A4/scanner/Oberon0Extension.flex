<YYINITIAL> {
  "["         { return sym(Terminals.LBRACK); }
  "]"         { return sym(Terminals.RBRACK); }
  "ARRAY"     { return sym(Terminals.ARRAY); }
  "RECORD"    { return sym(Terminals.RECORD); }
}
