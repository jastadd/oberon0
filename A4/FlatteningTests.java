import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import AST.Module;
import testdrivers.*;


public class FlatteningTests extends TestSuite {
	@Test
	public void nameConflict() { checkFlattening("flattening/NameConflict"); }

	@Test
	public void nameConflict2() { checkFlattening("flattening/NameConflict2"); }

	@Test
	public void nameConflict3() { checkFlattening("flattening/NameConflict3"); }

	protected void checkFlattening(String filename) {
		Module m = checkSyntaxFile(filename + ".ob");
		assertEquals(0, m.errors().size());
		
		// Do flatten
		Module flat1 = m.flatten();
		Module m2 = checkSyntaxString(flat1.prettyPrint());
		assertTrue(m2.isAlreadyFlat());
		assertEquals(0, m2.errors().size());

		// Try to flatten the already flatten program
		for (int i = 0; i < 10; i++) {
			Module flat2 = m2.flatten();
			Module m3 = checkSyntaxString(flat2.prettyPrint());
			assertTrue(m3.isAlreadyFlat());
			assertEquals(0, m3.errors().size());
			m2 = m3;
		}
	}
}
