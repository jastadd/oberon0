#!/bin/sh
if [ $# -eq 0 ] 
then
	java -cp .:../tools/beaver.jar Compiler program.ob 
else
	java -cp .:../tools/beaver.jar Compiler $1
fi
