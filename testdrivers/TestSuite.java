package testdrivers;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;

import AST.*;
import AST.Module;

abstract public class TestSuite {
	protected Module checkSyntaxFile(String filename) {
		try {
			Reader r = new FileReader(new File("testfiles/", filename));
			return checkSyntax(r, "Error when parsing the file " + filename);
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
			return null;
		}
	}
	
	protected Module checkSyntaxString(String program) {
		Reader r = new StringReader(program);
		return checkSyntax(r, "Error when parsing string " + program);
	}
	
	protected Module checkSyntax(Reader reader, String message) {
		PrintStream err = System.err;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		
		try {
			System.setErr(ps);
			Module m = parse(reader);
			if (os.size() > 0) {
				// The parser should not recover from anything, such errors
				// should not be put in a test case (otherwise it should be stated explicitly).
				fail("Parser recovery:\n" + os.toString());
			}
			return m;
		} catch(Exception e) {
			System.setErr(err);
			fail(message + ":\n " + e.getMessage() + "\n" + os.toString());
			return null; // This line is required to remove compile errors...  
		}
	}
	
	protected void checkNoErrors(String filename) {
		Module m = checkSyntaxFile(filename + ".ob");
			
		StringBuilder sb = new StringBuilder();
		for (ErrorMessage e : m.errors()) {
			sb.append(e + "\n");
		}
		
		assertEquals("", sb.toString());
	}
	
	protected void checkErrors(String filename) {
		Module m = checkSyntaxFile(filename + ".ob");
		
		StringBuilder sb = new StringBuilder();
		for (ErrorMessage e : m.errors()) {
			sb.append(e + "\n");
		}
		
		assertEquals(readFileAsString(filename + ".err"), sb.toString());
	}
	
	protected void checkPrettyPrinter(String filename) {
		Module m = checkSyntaxFile(filename + ".ob");
		assertEquals(0, m.errors().size());		
		assertEquals(readFileAsString(filename + ".pretty"), m.prettyPrint());
	}
	
		private Module parse(Reader reader) throws IOException, beaver.Parser.Exception{
		Oberon0Scanner scanner = new Oberon0Scanner(reader);
		Oberon0Parser parser = new Oberon0Parser();
		return (Module)parser.parse(scanner);
	}
	
	
	private static String readFileAsString(String filename) {
		File file = new File("testfiles/", filename);
		BufferedInputStream f = null;
    	byte[] buffer = null;
	    try {
	    	buffer = new byte[(int) file.length()];
		    f = new BufferedInputStream(new FileInputStream(file));
	        f.read(buffer);
	    } catch (IOException e) { 
	    	fail(e.getMessage());
	    } finally {
	        if (f != null) try { f.close(); } catch (IOException ignored) { }
	    } 
	    return new String(buffer);
	}
}
