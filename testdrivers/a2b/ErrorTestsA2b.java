package testdrivers.a2b;

import org.junit.Test;

import testdrivers.TestSuite;


public class ErrorTestsA2b extends TestSuite {
	/** Type errors, L1 */
	@Test
	public void assignmentToConst() { checkErrors("negative/type_errors/L1/4_AssignmentToConst"); }

	@Test
	public void assignmentToType() { checkErrors("negative/type_errors/L1/4_AssignmentToType"); }
	
	@Test
	public void combiningIntegerAndBoolean() { checkErrors("negative/type_errors/L1/7_CombiningIntegerAndBoolean"); }

	@Test
	public void typeChecking() { checkErrors("negative/type_errors/L1/5_TypeChecking"); }

	@Test
	public void typeCheckingAndOp() { checkErrors("negative/type_errors/L1/3_TypeCheckingAndOp"); }
	
	@Test
	public void typeCheckingIfStmt() { checkErrors("negative/type_errors/L1/3_TypeCheckingIfStmt"); }
	@Test
	public void typeCheckingIfStmt2() { checkErrors("negative/type_errors/L1/4_TypeCheckingIfStmt2"); }

	@Test
	public void typeCheckingPlusOp() { checkErrors("negative/type_errors/L1/4_TypeCheckingPlusOp"); }	
	
	@Test
	public void typeCheckingWhileStmt() { checkErrors("negative/type_errors/L1/4_TypeCheckingWhileStmt"); }
	
	
	/** Type errors, L2 */
	@Test
	public void typeCheckingCaseStmt() { checkErrors("negative/type_errors/L2/5_TypeCheckingCaseStmt"); }
	@Test
	public void typeCheckingCaseStmt2() { checkErrors("negative/type_errors/L2/6_TypeCheckingCaseStmt2"); }
	@Test
	public void typeCheckingCaseStmt3() { checkErrors("negative/type_errors/L2/7_TypeCheckingCaseStmt3"); }
	@Test
	public void typeCheckingCaseStmt4() { checkErrors("negative/type_errors/L2/3_TypeCheckingCaseStmt4"); }
	
	@Test
	public void typeCheckingForStmt() { checkErrors("negative/type_errors/L2/4_TypeCheckingForStmt"); }
	@Test
	public void typeCheckingForStmt2() { checkErrors("negative/type_errors/L2/4_TypeCheckingForStmt2"); }
	@Test
	public void typeCheckingForStmt3() { checkErrors("negative/type_errors/L2/3_TypeCheckingForStmt3"); }
	@Test
	public void typeCheckingForStmt4() { checkErrors("negative/type_errors/L2/4_TypeCheckingForStmt4"); }
	@Test
	public void typeCheckingForStmt5() { checkErrors("negative/type_errors/L2/4_TypeCheckingForStmt5"); }
	@Test
	public void typeCheckingForStmt6() { checkErrors("negative/type_errors/L2/4_TypeCheckingForStmt6"); }
}
