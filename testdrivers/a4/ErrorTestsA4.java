package testdrivers.a4;

import org.junit.Test;

import testdrivers.TestSuite;


public class ErrorTestsA4  extends TestSuite {
	/** Name errors, L4 */
	@Test
	public void recordDecl() { checkErrors("negative/name_errors/L4/4_RecordDecl"); }
	@Test
	public void recordDecl2() { checkErrors("negative/name_errors/L4/3_RecordDecl2"); }


	/** Type errors, L4 */
	@Test
	public void arrayAccess() { checkErrors("negative/type_errors/L4/8_ArrayAccess"); }
	@Test
	public void arrayAccess2() { checkErrors("negative/type_errors/L4/8_ArrayAccess2"); }
	@Test
	public void arrayAccess3() { checkErrors("negative/type_errors/L4/8_ArrayAccess3"); }
	
	@Test
	public void arrayAccessIntegerType() { checkErrors("negative/type_errors/L4/5_ArrayAccessIntegerType"); }
	
	@Test
	public void arrayDecl() { checkErrors("negative/type_errors/L4/2_ArrayDecl"); }
	@Test
	public void arrayDecl2() { checkErrors("negative/type_errors/L4/3_ArrayDecl2"); }
	
	@Test
	public void combiningArraysAndRecords() { checkErrors("negative/type_errors/L4/17_CombiningArraysAndRecords"); }
	@Test
	public void combiningArraysAndRecords2() { checkErrors("negative/type_errors/L4/17_CombiningArraysAndRecords2"); }
	@Test
	public void combiningArraysAndRecords3() { checkErrors("negative/type_errors/L4/17_CombiningArraysAndRecords3"); }
	
	@Test
	public void illegalAssignment() { checkErrors("negative/type_errors/L4/7_IllegalAssignment"); }
	@Test
	public void illegalAssignment2() { checkErrors("negative/type_errors/L4/7_IllegalAssignment2"); }
	@Test
	public void illegalAssignment3() { checkErrors("negative/type_errors/L4/6_IllegalAssignment3"); }
	
	@Test
	public void outOfBounds() { checkErrors("negative/type_errors/L4/9_OutOfBounds"); }
	@Test
	public void outOfBounds2() { checkErrors("negative/type_errors/L4/9_OutOfBounds2"); }
	
	@Test
	public void procPars() { checkErrors("negative/type_errors/L4/10_ProcPars"); }
	@Test
	public void procPars2() { checkErrors("negative/type_errors/L4/10_ProcPars2"); }
	@Test
	public void procPars3() { checkErrors("negative/type_errors/L4/15_ProcPars3"); }
	@Test
	public void procPars4() { checkErrors("negative/type_errors/L4/15_ProcPars4"); }
	
	@Test
	public void recordAccess() { checkErrors("negative/type_errors/L4/10_RecordAccess"); }
	@Test
	public void recordAccess2() { checkErrors("negative/type_errors/L4/10_RecordAccess2"); }
}
