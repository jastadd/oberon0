package testdrivers.a4;

import org.junit.Test;

import testdrivers.TestSuite;


public class SloaneTestsA4 extends TestSuite {
	@Test
	public void arrayAndRecStressTest1() { checkNoErrors("sloane/A4/ArrayAndRecStressTest1"); }

	@Test
	public void arrayAndRecStressTest2() { checkNoErrors("sloane/A4/ArrayAndRecStressTest2"); }
	
	@Test
	public void assmblyTest() { checkNoErrors("sloane/A4/AssmblyTest"); }

	@Test
	public void compositeObjTest() { checkNoErrors("sloane/A4/CompositeObjTest"); }
	
	@Test
	public void procTest1() { checkNoErrors("sloane/A4/ProcTest1"); }
	
	@Test
	public void sorting() { checkNoErrors("sloane/A4/Sorting"); }

	/* The following test contains an error: a call to a procedure that not has been declared yet. 
	@Test
	public void staticLinkTest1() { checkNoErrors("sloane/StaticLinkTest1"); }
	 */
}
