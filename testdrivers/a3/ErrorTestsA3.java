package testdrivers.a3;

import org.junit.Test;

import testdrivers.TestSuite;


public class ErrorTestsA3 extends TestSuite {
	/** Type errors, L3 */
	@Test
	public void constDecl2() { checkErrors("negative/type_errors/L3/5_ConstDecl2"); }
	@Test
	public void constDecl3() { checkErrors("negative/type_errors/L3/3_ConstDecl3"); }
	@Test
	public void constDecl4() { checkErrors("negative/type_errors/L3/3_ConstDecl4"); }

	@Test
	public void procCallVarPars() { checkErrors("negative/type_errors/L3/9_ProcCallVarPars"); }
	@Test
	public void procCallVarPars2() { checkErrors("negative/type_errors/L3/9_ProcCallVarPars2"); }
	@Test
	public void procCallVarPars3() { checkErrors("negative/type_errors/L3/10_ProcCallVarPars3"); }

	@Test
	public void userDefinedProcCallPars() { checkErrors("negative/type_errors/L3/10_UserDefinedProcCallPars"); }
	@Test
	public void userDefinedProcCallPars2() { checkErrors("negative/type_errors/L3/10_UserDefinedProcCallPars2"); }
	@Test
	public void userDefinedProcCallPars3() { checkErrors("negative/type_errors/L3/10_UserDefinedProcCallPars3"); }
	@Test
	public void userDefinedProcCallPars4() { checkErrors("negative/type_errors/L3/10_UserDefinedProcCallPars4"); }

	@Test
	public void predefinedProcPars() { checkErrors("negative/type_errors/L3/4_PredefinedProcPars"); }
	@Test
	public void predefinedProcPars2() { checkErrors("negative/type_errors/L3/4_PredefinedProcPars2"); }
	@Test
	public void predefinedProcPars3() { checkErrors("negative/type_errors/L3/4_PredefinedProcPars3"); }
	@Test
	public void predefinedProcPars4() { checkErrors("negative/type_errors/L3/4_PredefinedProcPars4"); }

	@Test
	public void nonProcedureCall() { checkErrors("negative/type_errors/L3/7_NonProcedureCall"); }
	@Test
	public void nonProcedureCall2() { checkErrors("negative/type_errors/L3/7_NonProcedureCall2"); }
	@Test
	public void nonProcedureCall3() { checkErrors("negative/type_errors/L3/7_NonProcedureCall3"); }
	@Test
	public void nonProcedureCall4() { checkErrors("negative/type_errors/L3/8_NonProcedureCall4"); }

	@Test
	public void procScopeRules() { checkErrors("negative/type_errors/L3/4_ProcScopeRules"); }
}
