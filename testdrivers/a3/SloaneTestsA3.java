package testdrivers.a3;

import org.junit.Test;

import testdrivers.TestSuite;


public class SloaneTestsA3 extends TestSuite {
	@Test
	public void conditionStressTest1() { checkNoErrors("sloane/A3/ConditionStressTest1"); }
	
	@Test
	public void count_to_x() { checkNoErrors("sloane/A3/Count_to_x"); }
	
	@Test
	public void divideByZero() { checkNoErrors("sloane/A3/DivideByZero"); }
	
	@Test
	public void factorial() { checkNoErrors("sloane/A3/Factorial"); }
	
	@Test
	public void GCD() { checkNoErrors("sloane/A3/GCD"); }

	/* The following test contains an error: a call to a procedure that not has been declared yet. 
	@Test
	public void staticLinkTest1() { checkNoErrors("sloane/StaticLinkTest1"); }
	 */
}
