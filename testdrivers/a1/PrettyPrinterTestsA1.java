package testdrivers.a1;

import org.junit.Test;

import testdrivers.TestSuite;


public class PrettyPrinterTestsA1 extends TestSuite {
	@Test
	public void caseStmt() { checkPrettyPrinter("pretty/A1/CaseStmt"); }

	@Test
	public void declarations() { checkPrettyPrinter("pretty/A1/Declarations"); }

	@Test
	public void expression() { checkPrettyPrinter("pretty/A1/Expression"); }

	@Test
	public void forStmt() { checkPrettyPrinter("pretty/A1/ForStmt"); }

	@Test
	public void ifStmt() { checkPrettyPrinter("pretty/A1/IfStmt"); }

	@Test
	public void whileStmt() { checkPrettyPrinter("pretty/A1/WhileStmt"); }
}
