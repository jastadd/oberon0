package testdrivers.a1;

import org.junit.Test;

import testdrivers.TestSuite;


public class ErrorTestsA1 extends TestSuite {
	/** Name errors, L1 */	
	@Test
	public void constCircularity() { checkErrors("negative/name_errors/L1/2_ConstCircularity"); }

	@Test
	public void constCircularity2() { checkErrors("negative/name_errors/L1/2_ConstCircularity2"); }

	@Test
	public void moduleName() { checkErrors("negative/name_errors/L1/2_ModuleName"); }	
	
	@Test
	public void multipleDeclarations() { checkErrors("negative/name_errors/L1/3_MultipleDeclarations"); }

	@Test
	public void multipleDeclarations2() { checkErrors("negative/name_errors/L1/3_MultipleDeclarations2"); }

	@Test
	public void multipleDeclarations3() { checkErrors("negative/name_errors/L1/2_MultipleDeclarations3"); }

	@Test
	public void typeCircularity() { checkErrors("negative/name_errors/L1/2_TypeCircularity"); }

	@Test
	public void unknownTypeName() { checkErrors("negative/name_errors/L1/2_UnknownTypeName"); }

	@Test
	public void unknownTypeName2() { checkErrors("negative/name_errors/L1/2_UnknownTypeName2"); }

	@Test
	public void varCircularity() { checkErrors("negative/name_errors/L1/2_VarCircularity"); }
	
	
	
	/** Type errors, L1 */
	// TODO: Remove test case???
	//@Test
	//public void constDecl() { checkErrors("negative/type_errors/L1/2_ConstDecl"); }
}
