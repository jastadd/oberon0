package testdrivers.a2a;

import org.junit.Test;

import testdrivers.TestSuite;


public class PrettyPrinterTestsA2a extends TestSuite {
	@Test
	public void procCall() { checkPrettyPrinter("pretty/A2a/ProcCall"); }

	@Test
	public void predeclaredProcedures() { checkPrettyPrinter("pretty/A2a/PredeclaredProcedures"); }

}
