package testdrivers.a2a;

import org.junit.Test;

import testdrivers.TestSuite;


public class ErrorTestsA2a extends TestSuite {
	/** Name errors, L3 */
	@Test
	public void procCall() { checkErrors("negative/name_errors/L3/10_ProcCall"); }

	@Test
	public void procCall2() { checkErrors("negative/name_errors/L3/9_ProcCall2"); }

	@Test
	public void procedureName() { checkErrors("negative/name_errors/L3/2_ProcedureName"); }

	@Test
	public void procScopeRules2() { checkErrors("negative/name_errors/L3/3_ProcScopeRules2"); }

	@Test
	public void variableAccess() { checkErrors("negative/name_errors/L3/11_VariableAccess"); }

	@Test
	public void variableAccess2() { checkErrors("negative/name_errors/L3/10_VariableAccess2"); }
}
