Oberon0 in JastAdd
=================================================
JastAdd implementation of Oberon0 for LDTA Tool Challenge 2011.
Version: 2011-08-31

System requirements
-------------------------------------------------
You need to have ant, javac (Java 6), and java installed.

All other tools (parser generators, jastadd, junit, etc.) are included
in this distribution.

Top level file organization
-------------------------------------------------

```
A1/                 Corresponds to L2, T1, T2
A2a/                Corresponds to L3, T1, T2
A2b/                Corresponds to L2, T1, T2, T3
A3/                 Corresponds to L3, T1, T2, T3
A4/                 Corresponds to L4, T1-T5a
README              This file
build.xml           Ant build script
referencecompiler-report.txt
                    The reference compiler does not comply with all tests.
                    This file explains which of the "errors" test cases
                    (checking of compile-time errors) succeed or fail for
                    the reference compiler.
                    It turns out that there are a couple of compile-time
                    errors that the reference compiler does not catch until
                    runtime, and a couple that it does not catch at all.
testfiles/          Oberon0 test cases
testdrivers/        JUnit files for running the tests.
    TestSuite.java    JUnit framework for a suite of test cases.
    /a1/              Test suites for A1
    /a2a/             Test suites for A2a
    ...
tools/              Tools required to create the compiler
```

The ant script at the top level has two targets, one for testing all artifacts
and one for cleaning them.

To test all artifacts (each will be cleaned before its tests are run):

    $ ant test

For each test suite, the number of test case failures and errors are reported.
They should all be 0. To check this more easily, grep for "Tests run:":

    $ ant test | grep "Tests run:"

To clean away all generated files:

    $ ant clean

Using the build script in a specific artifact
-------------------------------------------------
First, we need to chose an artifact, for example, A1, and go to that directory.

    $ cd A1

Then, we can run the test cases

    $ ant test

And create a jar file, called ```Oberon0-compiler-A1.jar```

    $ ant jar

Finally, clean the directory

    $ ant clean

(The clean target removes all generated files recursively in the artifact
directory, including those created by the Oberon0 compiler, for example,
all ```<filename>.errors``` files)

The above ant targets works for all artifacts.

Using the generated Oberon0 compiler
-------------------------------------------------
When we have a jar file, we can run it as follows:

    $ java -jar Oberon0-compiler-A1.jar <filename>

If the file ```<filename>``` contains any errors, the line of the first error will
be printed on standard output. Note that A1 should be replaced with the
desired artifact (A1, A2a, A2b, A3, A4).

Note that only A4 generates C-code.

When running the compiler on a file called ```<filename>.ob```, the following files
are created:

    <filename>.errors       Compile-time error messages, if any
    <filename>_pp.ob        Pretty printed version
    <filename>_lifted.ob    Flattened Oberon0 program.
                            (Only for A4 and no compile-time errors)
    <filename>.c            C program. (Only for A4 and no compile-time errors)

For example, if we run the compiler for artifact A1

    $ java -jar Oberon0-compiler-A1.jar directory/test.ob

Then the files ```directory/test.errors``` and ```directory/test_pp.ob``` will be created.

Typical file organization for an artifact
-------------------------------------------------

    Compiler.java       The main program of the compiler
    build.xml           Ant build script
    parser/             The parser specification
    scanner/            The scanner specification
    spec/               The JastAdd specification
        Errors.jrag     Attributes defining error messages
        *.jrag          Other attributes, e.g., name analysis, type analysis, ...
        *.jadd          Intertype method declarations, e.g., prettyprinting.
        *.ast           Abstract grammar
    testfiles           A symbolic link to the top level directory "testfiles".
    testdrivers         A symbolic link to the top level directory "testdrivers".
                        All JUnit test suites in "testdrivers" are compiled, but not
                        necessarily run, for this artifact. This allows us to run
                        tests created for a "lower" artifact, e.g., A1, also on
                        a "higher" artifact, e.g., A2. The build.xml file lists
                        which test suites to run.


The artifact A4 also contains a specific testdriver ```FlatteningTests.java```.
It is not placed in the common testdrivers directory because it can only
be compiled in A4.

Test case organization
-------------------------------------------------
The proposed structure for test cases (positive/L1, ...) is not yet used.
Instead, we use a structure based on the artifacts A1, ...

    testfiles/
      errors/           Test programs for compile-time errors (negative cases)
        A1/
          *.ob              Oberon0 program
          *.err             Expected output from compilation
        A2a/
        ...

      flattening/        Test programs for flattening procedures.
                         These are only relevant for A4, and structured specifically
                         for the JastAdd implementation to check its particular name
                         mangling. Other implementations with other name mangling
                         schemes should probably use other test cases. We test if
                         compiling these programs result in flattened programs
                         without compile-time errors.

      pretty/            Test programs for prettyprinting. These programs do not
                         contain any compile-time errors.
        A1/
          *.ob             Oberon0 program
          *.pretty         Expected prettyprinted program.
        ...

      sloane/            Test programs originally taken from Tony Sloane's
                         implementation of Oberon0 in Kiama. For testing dynamic
                         semantics. So far, we have no automation of these tests
                         for our compiler, but we have checked them manually.
                         We do have automated tests for checking that the programs
                         contain no compile-time errors.

                         The tests have been adjusted to comply to the reference
                         compiler. Negative test cases were removed because there
                         are already corresponding test cases in our "errors" dir.
                         The testcase StaticLinkTest1 was also removed, because
                         Oberon0 does not support static links.

        *.ob                Oberon0 program
        *.in                Input to the corresponding program
        *.out               Expected output from running the program

Adding a new test case
-------------------------------------------------
- Add the Oberon0 file and expected output files in some appropriate place in
  ```testfiles/```.
- Change an appropriate JUnit testdriver (```testdrivers/<artifact>/*Tests*.java```)
  to include the test case.
- Run ```ant test```.

If you add a whole new testdriver, you need to change the ```build.xml``` file for
each appropriate artifact, to include it.

Possibly, we might use some more automatic scheme later, so you could just add
the test case files, and wouldn't have to update any drivers.

